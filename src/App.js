import React, {useState} from 'react';
import './styles/App.css'
import CameraComponent from './CameraComponent';
import DropdownSelection from './DropdownSelection';
import 'react-html5-camera-photo/build/css/index.css';

function App() {
  const [icon, setIcon] = useState('');
  const [camera, setCamera] = useState('environment');

  function setValueCamera() {
    if(camera === "user") {
        setCamera("environment")
    } else {
        setCamera("user")
    }
  }

  return (
    <div className="App" style={{ textAlign: 'center' }}>
      <div id="header">
        <DropdownSelection setValueImg={setIcon}/>
        <img onClick={() => setValueCamera()} id="sync-camera" src="https://www.flaticon.com/premium-icon/icons/svg/726/726319.svg"/>
      </div>
      <div id="capture">
        <img id="img-select" src={icon}/>
        <CameraComponent camera={camera} icon={icon}/>
        <canvas id="canvas" style={{display: 'none'}}></canvas>
      </div>
    </div>
  );
}

export default App;
