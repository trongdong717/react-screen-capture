import React from "react";
import ReactCustomFlagSelect from "react-custom-flag-select";
import "react-custom-flag-select/lib/react-custom-flag-select.min.css";

let valueSelect = [
    {
        id: "0",
        flag: "https://image.flaticon.com/icons/png/512/1827/1827957.png"
    },
    {
        id: "1",
        flag: "https://image.flaticon.com/icons/svg/2949/2949887.svg"
    },
    {
        id: "2",
        flag: "https://image.flaticon.com/icons/svg/2979/2979270.svg"
    },
    {
        id: "3",
        flag: "https://image.flaticon.com/icons/svg/2979/2979265.svg"
    },
    {
        id: "4",
        flag: "https://image.flaticon.com/icons/svg/2971/2971420.svg"
    },
    {
        id: "5",
        flag: "https://image.flaticon.com/icons/svg/2971/2971414.svg"
    },
    {
        id: "6",
        flag: "https://image.flaticon.com/icons/svg/2971/2971418.svg"
    }
];

class DropdownSelection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "0"
    };
  }

  handlePhoneChange(id) {
    this.setState({ id });
    const imgUrl = id != 0 ? valueSelect.find(item => item.id === id).flag : "";
    this.props.setValueImg(imgUrl)
  }

  render() {
    const { id } = this.state;
    return (
      <div id="drop-select-img" style={{marginLeft: "10px", width: "30px"}}>
        <ReactCustomFlagSelect
          value={id}
          optionList={valueSelect}
          onChange={id => this.handlePhoneChange(id)}
        />
      </div>
    );
  }
}

export default DropdownSelection
