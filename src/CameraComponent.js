import React, {useEffect} from 'react';
import Camera from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';

function CameraComponent({icon, camera}) {
    const width = window.innerWidth;
    const height = window.innerHeight || window.screen.height;
    useEffect(() => {
        const videoDom = document.getElementsByTagName('video');
        if(videoDom && videoDom[0]) {
          videoDom[0].style.height = height + "px"
        }
        showImgToVideo ()
    })

    function showImgToVideo () {
        let display = icon ? "block" : "none";
        document.getElementById("img-select").style.display = display;
    }

    function handleTakePhoto (dataUri) {
        const canvas = document.getElementById("canvas");
        const videoDom = document.querySelector("video");
        canvas.width = width;
        canvas.height = height;
        const scaleWidth = width / videoDom.videoWidth;
        const scaleHeight = height / videoDom.videoHeight;
        const ctx = canvas.getContext("2d");
        ctx.scale(scaleWidth, scaleHeight);
        const img = new Image;
        img.onload = function(){
            ctx.drawImage(img,0,0);
        };
        img.src = dataUri;
        if (icon) {
            setTimeout(() => { insertImageToCanvas(scaleWidth, scaleHeight) }, 0);
        } else {
            setTimeout(() => { downloadImg() }, 0);
        }
    }
    function downloadImg() {
        const canvas = document.getElementById("canvas");
        const link = document.createElement('a');
        link.download = Date.now() + '.jpeg';
        link.href = canvas.toDataURL('image/jpeg');
        link.click();
    }

    function insertImageToCanvas(scaleWidth, scaleHeight) {
        const canvas = document.getElementById("canvas");
        const imgSlected = document.getElementById('img-select');
        const ctx = canvas.getContext("2d");
        ctx.scale(1 / scaleWidth, 1 / scaleHeight)
        var img = new Image;
        img.onload = function(){
          const x = imgSlected.offsetLeft;
          const y = imgSlected.offsetTop;
          const w = imgSlected.width;
          const h = imgSlected.height;
          ctx.drawImage(img, x, y, w, h);
          downloadImg()
        };
        img.setAttribute('crossOrigin', 'anonymous')
        img.src = icon;
    }
    return (
        <Camera
            onTakePhotoAnimationDone = { (dataUri) => { handleTakePhoto(dataUri); } }
            idealFacingMode = {camera}
            idealResolution = {{width: width, height: height}}
            imageType = {'jpg'}
            imageCompression = {1}
            isMaxResolution = {true}
            isImageMirror = {false}
            isSilentMode = {false}
            isDisplayStartCameraError = {false}
            isFullscreen = {true}
            sizeFactor = {1}
        />
    )
}

export default CameraComponent